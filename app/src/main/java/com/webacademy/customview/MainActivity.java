package com.webacademy.customview;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;

import com.webacademy.customview.views.EqualizerView;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class MainActivity extends AppCompatActivity {

    private EqualizerView mEqualizer;
    private TextureView mTextureView;
    private ScheduledExecutorService mService;
    private Renderer mRenderer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mEqualizer = (EqualizerView) findViewById(R.id.equalizer);
        mTextureView = (TextureView) findViewById(R.id.texture_view);
        mTextureView.setOpaque(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mRenderer = new Renderer();
        mTextureView.setSurfaceTextureListener(mRenderer);
        mRenderer.start();
        if (mService == null || mService.isShutdown()) {
            mService = Executors.newSingleThreadScheduledExecutor();

        }
        mService.scheduleWithFixedDelay(new Runnable() {
            boolean isNegative;
            Random random = new Random();

            @Override
            public void run() {
                if (mEqualizer.isRunning()) {
                    int value = random.nextInt(11);
                    value = isNegative ? -1 * value : value;
                    mEqualizer.putNewValue(value);
                    mRenderer.putNewValue(value);
                    isNegative = !isNegative;
                } else {
                    mEqualizer.start();
                }
            }
        }, 0, 16, TimeUnit.MILLISECONDS);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mService.shutdown();
        mRenderer.halt();
        try {
            mRenderer.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /**
     * Just for fun. Example based on TextureView + Path
     */
    private static class Renderer extends Thread implements TextureView.SurfaceTextureListener {

        private static final String TAG = Renderer.class.getSimpleName();


        private static final int DEF_STEP_MAX_HEIGHT = 10;
        private static final int DEF_STEPS_COUNT = 40;
        private static final float DEF_STROKE_WIDTH = 2.0f;
        private static final float DEF_CORNER_RADIUS = 5.0f;
        private static final int DEF_LEVEL_LINE_COLOR = Color.RED;
        private static final int DEF_EQUALIZER_LINE_COLOR = Color.BLACK;


        private int mEqualizerMaxHeight;
        private int mEqualizerStepsCount;
        private int mEqualizerStepHeight;
        private int mEqualizerStepWidth;

        private Object mLock = new Object();
        private SurfaceTexture mSurfaceTexture;
        private boolean mDone;

        private int mWidth;
        private int mHeight;

        private int[] mArrayOfValues;


        private Paint mPaint;
        private Path mPath;

        private CyclicBarrier mBarrier = new CyclicBarrier(1);


        public Renderer() {
            super("Render thread");
            mEqualizerStepsCount = DEF_STEPS_COUNT;
            mArrayOfValues = new int[DEF_STEPS_COUNT];
            Arrays.fill(mArrayOfValues, 0);
            mPaint = new Paint();
            mPaint.setAntiAlias(true);
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeWidth(DEF_STROKE_WIDTH);
            mPaint.setPathEffect(new CornerPathEffect(DEF_CORNER_RADIUS));
            mPath = new Path();
        }

        public void putNewValue(@IntRange(from = -DEF_STEP_MAX_HEIGHT, to = DEF_STEP_MAX_HEIGHT) int value) {
            System.arraycopy(mArrayOfValues, 1, mArrayOfValues, 0, mEqualizerStepsCount - 1);
            mArrayOfValues[mEqualizerStepsCount - 1] = value;
            mBarrier.reset();
        }

        @Override
        public void run() {
            while (!mDone) {
                SurfaceTexture surfaceTexture = null;
                synchronized (mLock) {
                    while (!mDone && (surfaceTexture = mSurfaceTexture) == null) {
                        try {
                            mLock.wait();
                        } catch (InterruptedException ie) {
                            throw new RuntimeException(ie);
                        }
                    }
                    if (mDone) {
                        break;
                    }
                }
                doAnimation();
            }
        }

        private void doAnimation() {

            Surface surface = null;
            synchronized (mLock) {
                SurfaceTexture surfaceTexture = mSurfaceTexture;
                if (surfaceTexture == null) {
                    Log.d(TAG, "ST null on entry");
                    return;
                }
                surface = new Surface(surfaceTexture);
            }

            while (!mDone) {
                Canvas canvas = surface.lockCanvas(null);
                if (canvas == null) {
                    Log.d(TAG, "lockCanvas() failed");
                    break;
                }
                if (canvas.getWidth() != mWidth || canvas.getHeight() != mHeight) {
                    initDraw(mWidth, mHeight);
                }
                canvas.drawColor(Color.WHITE);
                mPaint.setColor(DEF_LEVEL_LINE_COLOR);
                canvas.drawLine(0, mEqualizerMaxHeight, mWidth, mEqualizerMaxHeight, mPaint);
                mPath.reset();
                int posX = 0;
                final int steps = mEqualizerStepsCount - 1;
                mPath.moveTo(posX, getBarHeight(mArrayOfValues[0]));
                for (int i = 1; i < steps; i++) {
                    posX += mEqualizerStepWidth;
                    mPath.lineTo(posX, getBarHeight(mArrayOfValues[i]));
                }
                final int lastX = Math.max(0, mWidth - posX);
                mPath.lineTo(posX + lastX, getBarHeight(mArrayOfValues[steps]));
                mPaint.setColor(DEF_EQUALIZER_LINE_COLOR);
                canvas.drawPath(mPath, mPaint);
                surface.unlockCanvasAndPost(canvas);
                try {
                    mBarrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }
            surface.release();
        }

        private int getBarHeight(int value) {
            return mEqualizerMaxHeight + value * mEqualizerStepHeight;
        }


        public void halt() {
            synchronized (mLock) {
                mDone = true;
                mLock.notify();
            }
        }

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture st, int width, int height) {
            mWidth = width;
            mHeight = height;
            initDraw(mWidth, mHeight);
            synchronized (mLock) {
                mSurfaceTexture = st;
                mLock.notify();
            }
        }

        public void onSurfaceTextureSizeChanged(SurfaceTexture st, int width, int height) {
            mWidth = width;
            mHeight = height;
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture st) {
            synchronized (mLock) {
                mSurfaceTexture = null;
            }
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture st) {
        }

        private void initDraw(int width, int height) {
            mEqualizerMaxHeight = height / 2;
            mEqualizerStepWidth = (int) Math.floor((float) width / (mEqualizerStepsCount - 1));
            final float space = width - mEqualizerStepWidth * mEqualizerStepsCount;
            final int additionalStepSpace = Math.round(space / mEqualizerStepsCount);
            if (space > 0) {
                mEqualizerStepWidth += additionalStepSpace;
            } else {
                mEqualizerStepWidth -= additionalStepSpace;
            }
            mEqualizerStepHeight = (int) Math.ceil((float) mEqualizerMaxHeight / DEF_STEP_MAX_HEIGHT);
        }

    }
}
