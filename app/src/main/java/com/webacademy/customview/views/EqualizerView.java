package com.webacademy.customview.views;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.ColorInt;
import android.support.annotation.IntRange;
import android.util.AttributeSet;
import android.view.View;

import com.webacademy.customview.R;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by rodin_000 on 8/28/2016.
 */

public class EqualizerView extends View {


    private static final int DEF_STEP_MAX_HEIGHT = 10;
    private static final int DEF_STEPS_COUNT = 20;
    private static final float DEF_STROKE_WIDTH = 1.0f;
    private static final int DEF_LEVEL_LINE_COLOR = Color.RED;
    private static final int DEF_EQUALIZER_LINE_COLOR = Color.BLACK;


    private Paint mPaint;

    private int mEqualizerMaxHeight;
    private int mEqualizerStepsCount;
    private int mEqualizerStepHeight;
    private int mEqualizerStepWidth;

    private int[] mArrayOfValues;

    private boolean mIsSizeChanged;
    private AtomicBoolean mIsRunning = new AtomicBoolean();

    private int mLevelLineColor;
    private int mEqualizerLineColor;


    private Handler mHandler = new Handler(Looper.getMainLooper()) {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (mIsRunning.get()) {
                System.arraycopy(mArrayOfValues, 1, mArrayOfValues, 0, mEqualizerStepsCount - 1);
                mArrayOfValues[mEqualizerStepsCount - 1] = msg.what;
                invalidate();
            }
        }
    };


    public EqualizerView(Context context) {
        this(context, null);
    }

    public EqualizerView(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.equalizerViewStyle);
    }

    public EqualizerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE);
        final TypedArray array = context.obtainStyledAttributes(attrs,
                R.styleable.EqualizerView, defStyleAttr,
                R.style.EqualizerViewStyle);
        int stepsCount = array.getInt(R.styleable.EqualizerView_equalizerStepsCount, DEF_STEPS_COUNT);
        mEqualizerStepsCount = Math.max(stepsCount, DEF_STEPS_COUNT);
        mArrayOfValues = new int[mEqualizerStepsCount];
        if (isInEditMode()) {
            Random random = new Random();
            for (int i = 0; i < mEqualizerStepsCount; i++) {
                int value = random.nextInt(DEF_STEP_MAX_HEIGHT + 1);
                mArrayOfValues[i] = value * (i % 2 == 0 ? -1 : 1);
            }
        } else {
            Arrays.fill(mArrayOfValues, 0);
        }
        mPaint.setStrokeWidth(array.getDimension(R.styleable.EqualizerView_lineStrokeWidth, DEF_STROKE_WIDTH));
        setLevelLineColor(array.getColor(R.styleable.EqualizerView_levelLineColor, DEF_LEVEL_LINE_COLOR));
        setEqualizerLineColor(array.getColor(R.styleable.EqualizerView_equalizerLineColor, DEF_EQUALIZER_LINE_COLOR));
        array.recycle();
    }

    private void setLevelLineColor(@ColorInt int color) {
        mLevelLineColor = color;
    }

    private void setEqualizerLineColor(@ColorInt int color) {
        mEqualizerLineColor = color;
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (w != oldw || h != oldh) {
            mIsSizeChanged = true;
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int sizeW = MeasureSpec.getSize(widthMeasureSpec);
        int modeW = MeasureSpec.getMode(widthMeasureSpec);
        int sizeH = MeasureSpec.getSize(heightMeasureSpec);
        int modeH = MeasureSpec.getMode(heightMeasureSpec);
        int minW = getMinimumWidth();
        int minH = getMinimumHeight();
        int resultW = 0;
        int resultH = 0;
        switch (modeW) {
            case MeasureSpec.EXACTLY: {
                resultW = sizeW;
                break;
            }
            case MeasureSpec.AT_MOST: {
                resultW = Math.max(minW, sizeW);
                break;
            }
            case MeasureSpec.UNSPECIFIED: {
                resultW = Math.min(minW, sizeW);
                break;
            }
        }
        switch (modeH) {
            case MeasureSpec.EXACTLY: {
                resultH = sizeH;
                break;
            }
            case MeasureSpec.AT_MOST: {
                resultH = Math.max(minH, sizeH);
                break;
            }
            case MeasureSpec.UNSPECIFIED: {
                resultH = Math.min(minH, sizeH);
                break;
            }
        }
        setMeasuredDimension(resultW, resultH);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int canvasWidth = canvas.getWidth();
        if (mIsSizeChanged) {
            if (!isInEditMode()) {
                initDraw(canvasWidth, canvas.getHeight());
                mIsSizeChanged = false;
            } else {
                initDraw(canvasWidth, 200);
                mIsSizeChanged = true;
            }
        }
        mPaint.setColor(mLevelLineColor);
        canvas.drawLine(0, mEqualizerMaxHeight, canvasWidth, mEqualizerMaxHeight, mPaint);
        mPaint.setColor(mEqualizerLineColor);
        int posX = 0;
        final int steps = mEqualizerStepsCount - 1;
        int lastX = 0;
        for (int i = 1; i < steps; i++) {
            posX += mEqualizerStepWidth;
            canvas.drawLine(lastX, getBarHeight(mArrayOfValues[i - 1]),
                    posX, getBarHeight(mArrayOfValues[i]), mPaint);
            lastX = posX;
        }
        lastX = Math.max(0, canvasWidth - posX);
        canvas.drawLine(posX, getBarHeight(mArrayOfValues[steps - 1]),
                posX + lastX, getBarHeight(mArrayOfValues[steps]), mPaint);


    }

    private void initDraw(int width, int height) {
        mEqualizerMaxHeight = height / 2;
        mEqualizerStepWidth = (int) Math.floor((float) width / (mEqualizerStepsCount - 1));
        final float space = width - mEqualizerStepWidth * mEqualizerStepsCount;
        final int additionalStepSpace = Math.round(space / mEqualizerStepsCount);
        if (space > 0) {
            mEqualizerStepWidth += additionalStepSpace;
        } else {
            mEqualizerStepWidth -= additionalStepSpace;
        }
        mEqualizerStepHeight = (int) Math.ceil((float) mEqualizerMaxHeight / DEF_STEP_MAX_HEIGHT);
    }


    private int getBarHeight(int value) {
        return mEqualizerMaxHeight + value * mEqualizerStepHeight;
    }


    public void putNewValue(@IntRange(from = -DEF_STEP_MAX_HEIGHT, to = DEF_STEP_MAX_HEIGHT) int value) {
        mHandler.sendEmptyMessage(value);
    }

    public void start() {
        mIsRunning.set(true);

    }

    public void stop() {
        mIsRunning.set(false);
    }

    public boolean isRunning() {
        return mIsRunning.get();
    }


    @Override
    protected void onDetachedFromWindow() {
        stop();
        mHandler.removeCallbacksAndMessages(null);
        super.onDetachedFromWindow();
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        if (!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        mArrayOfValues = savedState.getArrayOfValues();
        mIsRunning.set(savedState.isRunning());
    }


    @Override
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        savedState.setIsRunning(mIsRunning.get());
        savedState.setArrayOfValues(mArrayOfValues);
        return savedState;
    }


    private static class SavedState extends BaseSavedState {

        private boolean mIsRunning;
        private int[] mArrayOfValues;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            mIsRunning = in.readByte() == 1;
            mArrayOfValues = new int[in.readInt()];
            in.readIntArray(mArrayOfValues);
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeByte((byte) (mIsRunning ? 1 : 0));
            out.writeInt(mArrayOfValues.length);
            out.writeIntArray(mArrayOfValues);
        }

        public boolean isRunning() {
            return mIsRunning;
        }

        public void setIsRunning(boolean isRunning) {
            this.mIsRunning = isRunning;
        }

        public int[] getArrayOfValues() {
            return mArrayOfValues;
        }

        public void setArrayOfValues(int[] arrayOfValues) {
            this.mArrayOfValues = arrayOfValues;
        }

        public static final Parcelable.Creator<SavedState> CREATOR =
                new Parcelable.Creator<SavedState>() {
                    public SavedState createFromParcel(Parcel in) {
                        return new SavedState(in);
                    }

                    public SavedState[] newArray(int size) {
                        return new SavedState[size];
                    }
                };
    }
}
